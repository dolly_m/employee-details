from Tkinter import *
from tkMessageBox import *
import sys

    
def addButton():
    empData = [e.get() for e in entries]
    Results.insert(END, '{:>20} {:>20} {:>20} {:>20}'.format(empData[0], empData[1], empData[2], empData[3]))

def savedDialog():
    showerror("Saved", "Your changes have been succefully saved.")

def saveButton():
    listBoxData = list(Results.get(0, END))
    listBoxData = [empD + '\n' for empD in listBoxData]
    with open('employee.txt', 'w') as textfile:
        textfile.writelines(listBoxData)
    savedDialog()



#Create GUI window
root = Tk()

root.wm_title('Employee Database')

#First Frame
first = LabelFrame(root, text="Enter Employee Details:")
first.grid(row=0, columnspan=7, sticky='WE', \
                 padx=5, pady=5, ipadx=5, ipady=5)

#labels = StringVar()
labels = ['EmployeeId', 'Name', 'Salary ($)', 'Department']

r=0
for l in labels:
    fLabels = Label(first, text=l+":", width=10)
    fLabels.grid(row=r+1, column=0, sticky='E', padx=5, pady=2)
    r= r+1

entries=[]
for i in range(4):
    fEntry = Entry(first)
    fEntry.grid(row=i+1, column=1, sticky='E', pady=2)
    entries.append(fEntry)


t=0
fButton1 = Button(first, text='Add', command=addButton)
fButton1.grid(row=5, column=t, padx=1, pady=2)




#Second Frame
second = LabelFrame(root, text="View Employee Details:")
second.grid(row=2, columnspan=7, sticky='WE', \
                 padx=5, pady=5, ipadx=5, ipady=5)


scrollbar = Scrollbar(second, orient=VERTICAL)
Results = Listbox(second, fg="blue", width=80, height=20, yscrollcommand=scrollbar.set, borderwidth=0, exportselection=0)
scrollbar.config(command=Results.yview)
scrollbar.pack(side=RIGHT, fill=Y)
Results.pack(side=LEFT)


with open("employee.txt", 'r') as file:
    for data in file:
        Results.insert(END, data.rstrip('\n'))
Results.pack()


def deleteButton():
    Results.delete(ANCHOR)
    Results.delete(ANCHOR)

fButton2 = Button(first, text='Delete', command=deleteButton)
fButton2.grid(row=5, column=t+1, padx=1, pady=2)


fButton3 = Button(first, text='Save', command=saveButton)
fButton3.grid(row=5, column=t+2, padx=1, pady=2)



root.minsize(20,20)
root.resizable(width=False, height=False)
mainloop()